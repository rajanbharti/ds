package test;

import main.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static main.ArrayProblems.nSubsets;
import static main.ArrayProblems.subsetSum;

/**
 * Created by rajan on 19/1/17.
 */
public class StringTest {

    @Test
    public void subsetTest(){
        int[] nums={1,2,4,5,9};
        System.out.println(nSubsets(nums,10));
        Arrays.sort(nums);
        System.out.println(subsetSum( nums,nums.length-1,10));
    }

    @Test
    public void permuatationsTest() {
        StringUtils.permutations("abc").forEach(System.out::println);
    }

    @Test
    public void anagramtest() {
        Assert.assertEquals(true, StringUtils.isAnagram("heol", "oleh"));
    }
}
