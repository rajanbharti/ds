package test;

import main.Heap;
import main.Sort;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class SortTest {

    int arr[] = {1, 4, 10, 2, 45, 11, 9};
    int res[] = {1, 2, 4, 9, 10, 11, 45};

    @Test
    public void heapSortTest() {

        assertArrayEquals(res, Heap.sort(arr));

    }

    @Test
    public void insertionSortTest() {
        assertArrayEquals(res, Sort.insertionSort(arr));
    }

    @Test
    public void selectionSortTest() {
        assertArrayEquals(res, Sort.selectionSort(arr));
    }

    @Test
    public void quickSortTest() {
        assertArrayEquals(res, Sort.quickSort(arr));
    }
}
