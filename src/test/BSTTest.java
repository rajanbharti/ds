package test;

import main.BST;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BSTTest {

    BST bst = new BST();

    /*
                4       Binary Search Tree
               / \
              2   5
            /  \   \
           1    3   6
                     \
                      7
                      \
                       8
     */

    @Before
    public void setup() {
        bst.insert(4);
        bst.insert(2);
        bst.insert(3);
        bst.insert(1);
        bst.insert(5);
        bst.insert(6);
        bst.insert(7);
        bst.insert(8);



    }

    @Test
    public void traversal(){
        bst.inOrder();
    }

    @Test
    public void sizeTest() {
         assertEquals(8,bst.size());
    }

    @Test
    public void levelTest(){
        assertEquals(2,bst.getLevel(3));
        assertEquals(4, bst.maxDepth());
        assertEquals(2,bst.minDepth());
    }

    @Test
    public void sumTest(){
        assertEquals(1,bst.leftLeavesSum());
        assertEquals(11,bst.rightLeavesSum());
        assertEquals(12,bst.leavesSum());
    }

    @Test
    public void nthNumberTest(){
        bst.levelOrder();
     //   assertEquals(8,bst.nthLargest(1));
     //   assertEquals(7,bst.nthLargest(2));
    }
}
