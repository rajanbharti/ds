package test;

import main.LinkedList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListTest {

    LinkedList list = new LinkedList();

    @Before
    public void setup() {
        list.append(2);
        list.append(7);
        list.append(1);
        list.append(10);
        list.append(9);
    }

    @Test
    public void sizeTest() {
        assertEquals(5, list.size());
    }

    @Test
    public void getDataTest() {
        assertEquals(1, list.getData(3));
        assertEquals(9, list.getData(5));
        assertEquals(-1, list.getData(9));
    }

    @Test
    public void traversalTest(){
        assertEquals("2,7,1,10,9",list.traverse());
        list.reverse();
        assertEquals("9,10,1,7,2",list.traverse());
    }

    @Test
    public void sortTest(){
        list.sort();
        assertEquals("1,2,7,9,10",list.traverse());
    }
}
