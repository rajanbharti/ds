package main;

import java.util.Collections;
import java.util.Map;

public class LinkedList {

    private Node head;

    private static class Node {
        int data;
        Node next;

        Node(int data) {
            this.data = data;
            next = null;
        }
    }

    public LinkedList() {
        head = null;
    }

    public void append(int data) {
        Node newNode = new Node(data);
        if (head == null)
            head = newNode;
        else {
            Node currentNode = head;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = newNode;
        }
    }

    public int size() {
        int nodeCount = 0;
        Node current = head;
        while (current.next != null) {
            nodeCount += 1;
            current = current.next;
        }
        return nodeCount + 1;
    }

    public void reverse() {
        head = reverse(head);
    }

    private Node reverse(Node node) {
        Node prev = null;
        Node current = node;
        Node next;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;

    }


    public boolean delete(int location) {
        if (location > size())
            return false;
        else {
            Node tempNode = head;
            int pos = 1;
            while (pos < location) {
                pos += 1;
                tempNode = tempNode.next;
            }
            tempNode = tempNode.next.next;
            return true;


        }
    }

    public int getData(int location) {
        if (location > size())
            return -1;
        else {
            Node tempNode = head;
            int pos = 1;
            while (pos < location) {
                pos += 1;
                tempNode = tempNode.next;
            }
            return tempNode.data;
        }
    }

    public String traverse() {
        StringBuilder sb = new StringBuilder();
        Node current = head;
        while (current != null) {
            sb.append(current.data);
            sb.append(",");
            current = current.next;
        }

        String result = sb.toString();
        return result.substring(0, result.length() - 1);
    }

    public void sort() {
        int maxIndex=size();
        while (maxIndex>0){
            sort(maxIndex-1);
            maxIndex--;
        }
    }

    private void sort(int index) {
        Node n = head;
        while (index >0) {
            if (n.data > n.next.data) {
                int tempData = n.next.data;
                n.next.data = n.data;
                n.data = tempData;
            }
            index--;
            n = n.next;
        }
    }
}
