package main;

import java.util.*;

public class KNumberSubset {
    private static int goal;
    private static int sum=0;
    private static List<Integer> subset = new ArrayList<>();
    private static int[] numbers;


    public static void findSubset(int[] list, int goal) {
        numbers = list;
        KNumberSubset.goal = goal;
        findSubset(0);
    }

    private static void findSubset(int startPoint) {
        if (sum == goal)
            System.out.println(subset + " :: " + sum);
        else
            for (int i = startPoint; i < numbers.length; i++) {
                sum = sum + numbers[i];
                if (sum > goal) {
                    sum = sum - numbers[i];
                    break;
                }
                subset.add((int) numbers[i]);
                findSubset(i + 1);
                sum = sum - numbers[i];
                subset.remove(subset.size() - 1);
            }
    }

    public static void main(String args[]) {
        int[] numbers = {1, 3, 5, 4,2, 6};
        Arrays.sort(numbers);
        findSubset(numbers, 10);
    }
}