package main;

public class Heap {

    private static int N;

    public static int[] sort(int arr[]) {
        heapify(arr);
        for (int i = N; i > 0; i--) {
            swap(arr, 0, i);
            N--;
            maxHeap(arr, 0);
        }
        return arr;
    }

    private static void heapify(int arr[]) {
        N = arr.length - 1;
        for (int i = N / 2; i >= 0; i--) {
            maxHeap(arr, i);
        }
    }

    private static void maxHeap(int arr[], int i) {
        int left = 2 * i ;
        int right = 2 * i + 1;
        int max = i;
        if ((left <= N) && (arr[left] > arr[i]))
            max = left;

        if ((right <= N) && (arr[right] > arr[max]))
            max = right;

        if (max != i) {
            swap(arr, i, max);
            maxHeap(arr, max);
        }
    }

    private static void swap(int arr[], int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
}
