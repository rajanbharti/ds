package main;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rajan on 19/1/17.
 */
public class StringUtils {

    public static Set<String> permutations(String s) {
        Set<String> stringList = new HashSet<>();
        permutations(s.toCharArray(), 0, s.length(), stringList);
        return stringList;
    }

    private static void permutations(char[] chars, int l, int r, Set<String> stringList) {
        if (l == r)
            stringList.add(new String(chars));
        else {
            for (int i = 0; i < r; i++) {
                swap(chars, l, i);
                permutations(chars, l + 1, r, stringList);
                swap(chars, l, i);
            }
        }
    }

    private static void swap(char[] chars, int src, int dst) {
        char t = chars[src];
        chars[src] = chars[dst];
        chars[dst] = t;
    }

    public static boolean isAnagram(String s1, String s2) {
        if (s1.length() != s2.length())
            return false;
        else {
            int[] arr = new int[26];
            char[] chars1 = s1.toCharArray();
            for (int i = 0; i < s1.length(); i++) {
                arr[chars1[i] - 97]++;
            }
            char[] chars2 = s2.toCharArray();
            for (int i = 0; i < s2.length(); i++) {
                if (arr[chars2[i] - 97] > 0)
                    arr[chars2[i] - 97]--;
            }
            int sum = arraySum(arr);
            if (sum == 0)
                return true;
            else
                return false;
        }
    }


    private static int arraySum(int[] arr) {
        int sum = 0;
        for (int n : arr)
            sum = sum + n;
        return sum;
    }
}
