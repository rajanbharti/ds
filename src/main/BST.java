package main;


public class BST {

    private Node root;
    private int[] sortedList;

    private class Node {
        int data;
        Node left;
        Node right;

        Node(int data) {
            this.data = data;
            left = null;
            right = null;
        }

        public boolean equals(Object o) {
            if ((o instanceof Node) && (((Node) o).data == this.data))
                return true;
            else
                return false;
        }
    }

    public BST() {
        root = null;
    }

    public void insert(int data) {
        Node newNode = new Node(data);
        if (root == null) {
            root = newNode;
            return;
        }

        Node currentNode = root;
        Node parent = null;
        while (true) {
            parent = currentNode;
            if (data < currentNode.data) {
                currentNode = currentNode.left;
                if (currentNode == null) {
                    parent.left = newNode;
                    return;
                }
            } else {
                currentNode = currentNode.right;
                if (currentNode == null) {
                    parent.right = newNode;
                    return;
                }
            }
        }

    }

    public int getLevel(int data) {
        int level = 0;
        Node current = root;
        while (current != null) {
            if (current.data == data) {
                break;
            } else {
                if (current.data > data)
                    current = current.left;
                else
                    current = current.right;
                level += 1;
            }
        }
        return level;
    }

    public int size() {
        return size(root);
    }

    private int size(Node n) {
        return (n == null) ? 0 : (size(n.left) + size(n.right) + 1);
    }

    public void inOrder() {
        inOrder(root);
    }

    private void inOrder(Node node) {
        if (node != null) {
            inOrder(node.left);
            System.out.println(node.data);
            inOrder(node.right);
        }
    }

    public void preOrder() {
        preOrder(root);
    }

    private void preOrder(Node node) {
        if (node != null) {
            System.out.println(node.data);
            preOrder(node.left);
            preOrder(node.right);
        }
    }

    public void postOrder() {
        postOrder(root);
    }

    private void postOrder(Node node) {
        if (node != null) {
            postOrder(node.left);
            postOrder(node.right);
            System.out.println(node.data);
        }
    }

    public void levelOrder() {
        int height = maxDepth();
        for (int i = 1; i <= height + 1; i++)
            printGivenLevel(root, i);
    }

    void printGivenLevel(Node root, int level) {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.data + " ");
        else if (level > 1) {
            printGivenLevel(root.left, level - 1);
            printGivenLevel(root.right, level - 1);
        }
    }

    public boolean delete(int id) {
        Node parent = root;
        Node current = root;
        boolean isLeftChild = false;
        while (current.data != id) {
            parent = current;
            if (current.data > id) {
                isLeftChild = true;
                current = current.left;
            } else {
                isLeftChild = false;
                current = current.right;
            }
            if (current == null) {
                return false;
            }
        }
        //if i am here that means we have found the node
        //Case 1: if node to be deleted has no children
        if (current.left == null && current.right == null) {
            if (current == root) {
                root = null;
            }
            if (isLeftChild) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        }
        //Case 2 : if node to be deleted has only one child
        else if (current.right == null) {
            if (current == root) {
                root = current.left;
            } else if (isLeftChild) {
                parent.left = current.left;
            } else {
                parent.right = current.left;
            }
        } else if (current.left == null) {
            if (current == root) {
                root = current.right;
            } else if (isLeftChild) {
                parent.left = current.right;
            } else {
                parent.right = current.right;
            }
        } else {

            //now we have found the minimum element in the right sub tree
            Node successor = getSuccessor(current);
            if (current == root) {
                root = successor;
            } else if (isLeftChild) {
                parent.left = successor;
            } else {
                parent.right = successor;
            }
            successor.left = current.left;
        }
        return true;
    }

    public Node getSuccessor(Node deleteNode) {
        Node successsor = null;
        Node successsorParent = null;
        Node current = deleteNode.right;
        while (current != null) {
            successsorParent = successsor;
            successsor = current;
            current = current.left;
        }
        //check if successor has the right child, it cannot have left child for sure
        // if it does have the right child, add it to the left of successorParent.
//		successsorParent
        if (successsor != deleteNode.right) {
            successsorParent.left = successsor.right;
            successsor.right = deleteNode.right;
        }
        return successsor;
    }

    public int leftLeavesSum() {
        return leftLeavesSum(root);
    }

    private int leftLeavesSum(Node root) {
        int sum = 0;
        if (root != null) {
            if (isLeaf(root.left))
                sum += root.left.data;
            else
                sum += leftLeavesSum(root.left);
            sum += leftLeavesSum(root.right);
        }
        return sum;
    }

    public int rightLeavesSum() {
        return rightLeavesSum(root);
    }

    private int rightLeavesSum(Node root) {
        int sum = 0;
        if (root != null) {
            if (isLeaf(root.right))
                sum += root.right.data;
            else
                sum += rightLeavesSum(root.right);
            sum += rightLeavesSum(root.left);
        }
        return sum;
    }

    public int leavesSum() {
        return (leftLeavesSum() + rightLeavesSum());
    }

    private Node copyBST(Node node) {
        if (node != null) {
            Node Node;
            Node = new Node(node.data);
            Node.left = copyBST(node.left);
            Node.right = copyBST(node.right);
            return Node;
        } else
            return null;
    }

    public int maxDepth() {
        return maxDepth(root) - 1;
    }

    private int maxDepth(Node node) {
        return (node == null) ? 0 : Math.max(maxDepth(node.left), maxDepth(node.right)) + 1;
    }

    public int minDepth() {
        return minDepth(root);
    }

    private int minDepth(Node node) {
        return (node == null) ? 0 : Math.min(minDepth(node.left), minDepth(node.right)) + 1;
    }


    private boolean isLeaf(Node node) {
        return node != null && node.left == null && node.right == null;
    }

    public int nthLargest(int n) {
        if (n > size())
            return -1;
        else {
            int data = 0;
            nthLargest(root, data, n);
            return data;
        }
    }

    private void nthLargest(Node node, int data, int n) {

    }


}
