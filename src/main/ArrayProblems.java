package main;

public class ArrayProblems {

    //Subset Sum Problem: Find all subsets from given array for a given sum


    public static int nSubsets(int[] numbers, int sum) {
        int[] dp = new int[sum + 1];
        dp[0] = 1;
        int currentSum = 0;
        for (int i = 0; i < numbers.length; i++) {
            currentSum += numbers[i];
            for (int j = Math.min(sum, currentSum); j >= numbers[i]; j--)
                dp[j] += dp[j - numbers[i]];
        }

        return dp[sum];
    }

    public static boolean subsetSum(int[] set, int n, int sum) {
        boolean[][] subset = new boolean[sum + 1][n+1];
        for (int i = 0; i < n; i++)
            subset[0][i] = true;

        for (int i = 1; i <= sum; i++)
            subset[i][0] = false;

        for (int i = 1; i <= sum; i++) {
            for (int j = 1; j <= n; j++) {
                subset[i][j] = subset[i][j - 1];
                if (i >= set[j-1])
                    subset[i][j] = subset[i][j] || subset[i - set[j - 1]][j - 1];
            }
        }
        return subset[sum][n];
    }
}


